import React,{useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';
import Icon from "react-native-vector-icons/AntDesign"
// import axios from "axios"

const Product=({navigation})=>{
  const [data, setData] = useState([])
  const [loader,setLoader]=useState(true)

const getList=async()=>{
  await fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then(json=>{
              console.log(json)
              setData(json)
              setLoader(false)
            })

//  await axios.get('https://fakestoreapi.com/products')
//  .then((resp)=>{
//    console.log(resp?.data, 'hi')
//    setData(resp.data)
//  })
}

  useEffect(()=>{
    getList()
  },[])

  const renderItem=({item})=>(
    <TouchableOpacity style={styles.flCard} onPress={()=>{
        navigation.navigate ('ProductDetail')
    }}>
      <Image
      source={{
        uri: `${item.image}`
      }}
      resizeMode={'contain'}
      style={{height: 180, width: 80}}
      />
      <View style={{paddingHorizontal:15}}>
      <Text ellipsizeMode={'tail'} numberOfLines={2} style={styles.itemTitle}>{item.title}</Text>
      <Text style={styles.rating}>{item.rating.rate} ⭐</Text>
      <Text style={styles.itemCategory}>{item.category}</Text>
      <Text ellipsizeMode={'tail'} numberOfLines={2} style={styles.itemDescription}>{item.description}</Text>
      <Text style={styles.itemPrice}> ₹{item.price}</Text>
      </View>
    </TouchableOpacity>
  )
  
 return(

   <View style={styles.container}>
     {/* <View style={styles.box}>
       <View style={{paddingLeft:8, margin:7, flexDirection: "row", alignItems:"center", justifyContent:"space-between"}}>
         <Text style={{color:"white", fontSize: 20}}>👋 Hello, Suraj</Text>
         <TouchableOpacity>
         <Icon name="search1" color="white" size={30}/>
         </TouchableOpacity>
       </View>
       <View style={{alignItems:"center", marginTop:20}}>
           <Text style={{color:"white", fontSize: 55}}>💸</Text>
           <Text style={{color:"white", fontSize: 20}}>Credits: 50</Text>
         </View>
     </View> */}
     {loader?
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <ActivityIndicator color={'blue'} size={30}/>
      </View>
      :<FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
      }
     
     <TouchableOpacity style={styles.button}>
     <Text style={styles.text}>💌 Share the love</Text>
     </TouchableOpacity>
   </View>
 )
}

export default Product;

const styles = StyleSheet.create({
  box: {
  backgroundColor:"blue",
  height: 200,
  },
  button: {
    backgroundColor: "blue",
    alignItems:"center",
    alignSelf:"center",
    margin: 30,
    paddingVertical: 15,
    paddingHorizontal: 90,
    borderRadius: 30
  },
  container:{
    flex: 1,
    backgroundColor: "white",
    justifyContent: "space-between",
  },
  text:{
    color: "white",
    fontSize: 18
  },
  flCard:{
    margin:"1%",
    elevation:5,
    backgroundColor:"white",
    flexDirection: "row",
    alignItems: "center",
    padding: '2%'
  },
  itemTitle:{
    color:"black",
    margin: 5,
    alignItems:"flex-start",
    width:200,
    fontSize: 18
  },
  itemCategory:{
    color:"orange",
    fontSize:17

  },
  rating:{
    fontSize: 18,
    color:"green",
    fontSize:20,
    fontWeight:"bold"
  },
  itemDescription:{
    fontSize: 17,
    width: 270,
    color: "grey",
  },
  itemPrice:{
    color:"green",
    fontWeight:'bold',
    fontSize: 20
  }
})
